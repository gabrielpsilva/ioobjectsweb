
$("#classpicker").change(function(){

  classes = JSON.parse( localStorage.classes );

  var selected =  $(this).find('option:selected').val();
  var selectedId = "";

  for (var key in classes) {
      if (selected.toString() == key){
        selectedId = classes[key];
    }
  }

  localStorage.clear();
  window.location.replace(window.location.pathname + "?class=" + selected + "&id=" + selectedId);

});

$(window).on('load', function() {

  ret =  getURLParameters(window.location.search);
  $('.btn.dropdown-toggle').text(ret.class.toString());

});


function getURLParameters(url){

    var result = {};
    var hashIndex = url.indexOf("#");
    if (hashIndex > 0)
       url = url.substr(0, hashIndex);
    var searchIndex = url.indexOf("?");
    if (searchIndex == -1 ) return result;
    var sPageURL = url.substring(searchIndex +1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        result[sParameterName[0]] = sParameterName[1];
    }
    return result;
}