import json

class IoField:

    def __init__(self):
        self.type = str()
        self.label = str()
        self.fieldKey = str()
        self.value = any

    def to_dict(self) -> dict:
        ret = dict()
        ret['type'] = self.type
        ret['label'] = self.label
        ret['fieldKey'] = self.fieldKey
        ret['value'] = self.value
        return ret

    def to_json(self, escape_chars=False) -> str:
        ret = json.dumps(self.to_dict())
        if escape_chars:
            return ret.replace("\"", "'")

        return ret


class IoObject:

    def __init__(self):
        self._id = str()
        self.objectClass = str()
        self.fields = list()

    def to_dict(self) -> dict:

        ret = dict()
        ret['_id'] = self._id
        ret['objectClass'] = self.objectClass
        ret['fields'] = list()

        for field in self.fields:
            new_field = dict()
            new_field['type'] = field.type
            new_field['label'] = field.label
            new_field['fieldKey'] = field.fieldKey
            new_field['value'] = field.value

            ret['fields'].append(new_field)

        return ret


    def to_json(self, escape_chars=False) -> str:
        ret = json.dumps(self.to_dict())
        if escape_chars:
            return ret.replace("\"", "'")

        return ret



    def get_field(self, fieldKey: str) -> IoField:
        for field in self.fields:
            if field.fieldKey == fieldKey:
                return field

        return IoField()

    def get_all_fields(self) -> list:

        if self.objectClass == 'IOOBJECT-CLASS':
            ret = list()
            for field in self.fields:
                if field.fieldKey not in ['name', 'pluralName', 'description']:
                    ret.append(field)
            return ret

        else:
            return self.fields


    def set_field(self, fieldKey: str, value: any):
        for i in range( len(self.fields) ):
            if self.fields[i].fieldKey == fieldKey:
                self.fields[i].value = value


    def add_field(self, type: str, field_key: str, label: str, value: any):

        self.fields.append( {'type': type,
                             'label': label,
                             'fieldKey' : field_key,
                             'value' : value
                             })

    @staticmethod
    def load_dict(ioObjects: list):

        ret = list()

        for ioObject in ioObjects:
            new_obj = IoObject()

            new_obj._id  = ioObject['id']
            new_obj.objectClass= ioObject['objectClass']

            for field in ioObject['fields']:
                new_field = IoField()
                new_field.type = field['type']
                new_field.label = field['label']
                new_field.fieldKey = field['fieldKey']
                new_field.value = field['value']

                new_obj.fields.append(new_field)

            ret.append(new_obj)

        return ret

