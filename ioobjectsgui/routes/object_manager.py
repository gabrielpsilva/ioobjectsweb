from flask import render_template, request, redirect, url_for

from ioobjectsgui.service.object_manager import ObjectManagerService as oms
from ioobjectsgui.service import IOApi as api
from ioobjectsgui import app


# GET : Lists all object classes
# POST: Creates new object class and list them
@app.route('/object-manager', methods=['POST', 'GET'])
def object_manager():

    if request.method == 'GET':
        response = api.get_objects(["IOOBJECT-CLASS"])
        return render_template('object-manager.jinja2', objects=response)

    if request.method == 'POST':

        new_o = oms.new_object_class(request.form.get('name'),
                                     request.form.get('pname'),
                                     request.form.get('description')
                                     )

        # TODO: Some validation on the return

        new_o = api.get_objects(["IOOBJECT-CLASS"])
        return redirect(url_for('object_manager', object_id=new_o), code=302)


# GET : Lists a object class
@app.route('/object-manager/id/<string:object_id>', methods=['GET'])
def object_manager_edit(object_id):

   object = oms.get_objects_by_id([object_id])
   return render_template('object-manager-edit.jinja2', object=object[0])


# POST: Creates new field for object class
@app.route('/object-manager/field/<string:object_id>', methods=['POST'])
def object_manager_add_field(object_id):

    ret = oms.new_field_to_object(id_object=object_id,
                            type=request.form.get('gridRadios'),
                            label=request.form.get('label'),
    )

    #TODO: Some validation on ret.
    #redirect to prevent resubmission on refresh
    return redirect(url_for('object_manager_edit', object_id=object_id), code=302)


# GET : Deletes field of a object
@app.route('/object-manager/field/<string:object_id>/<string:field_key>', methods=['GET'])
def object_manager_field_delete(object_id, field_key):

    ret = oms.delete_field_of_object(object_id, field_key)
    return redirect(url_for('object_manager_edit', object_id=ret._id), code=302)


@app.route('/object-manager/delete/<string:object_id>/', methods=['GET'])
def object_manager_delete(object_id):

    oms.delete_object(object_id)
    return redirect(url_for('object_manager'), code=302)
