from flask import render_template
from ioobjectsgui import app

from ioobjectsgui.routes import object_manager, data_objects


@app.route('/')
def index():
    return render_template('index.jinja2')
