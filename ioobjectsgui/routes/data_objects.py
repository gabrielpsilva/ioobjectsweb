from flask import render_template, request, redirect, url_for
import requests
import json

from ioobjectsgui.ioobjects import IoObject
from ioobjectsgui.service import api_urls as urls
from ioobjectsgui.service import IOApi as api
from ioobjectsgui.service.data_object import DataObjectsService as dos

from ioobjectsgui import app


@app.route('/data-objects', methods=['GET'])
def data_objects():

    response = api.get_objects()

    classes_map = dict()
    classes = list()
    data = list()
    for o in response:
        if o.objectClass == "IOOBJECT-CLASS":
            classes_map[o.get_field('name').value] = o._id
            classes.append(o)
        else:
            data.append(o)

    classes_map = json.dumps(classes_map)
    object_id = request.args.get('id')

    if object_id == None:
        return render_template('object-data.jinja2', classes=classes, map=classes_map, data=data, selected=None)

    selected = ""
    for o in response:
       if o._id == object_id:
           selected =  o

    return render_template('object-data.jinja2', classes=classes, map=classes_map, data=data, selected=selected)

@app.route('/data-objects', methods=['POST'])
def data_objects_new_object():

    response = requests.get(urls["object_url"] + request.args.get('id') )

    if response.status_code == 202:
        response = json.loads(response.content)
        response = IoObject.load_dict([response])
        response = response[0]

    new_obj = IoObject()

    new_obj.objectClass = request.args.get('class')
    new_obj.fields = response.get_all_fields()

    for fieldname in request.form:
        print(f'{fieldname} --> {request.form.get(fieldname)} ')
        new_obj.set_field(fieldname, request.form.get(fieldname))

    ret = dos.save_new_object(new_obj)

    json.dumps(ret[0].to_dict(), indent=2)

    return redirect(url_for('data_objects', code=302))