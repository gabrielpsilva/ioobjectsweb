import requests
import json

from ioobjectsgui.ioobjects import IoObject
from ioobjectsgui.service.object_manager import api_urls

class DataObjectsService:

    @staticmethod
    def save_new_object(ioObject: IoObject) -> IoObject:

        heads = {'Content-type': 'application/json'}
        response = requests.post(api_urls['base_url'], data=ioObject.to_json(), headers=heads)
        response = json.loads(response.content)
        response = IoObject.load_dict([response])

        return response