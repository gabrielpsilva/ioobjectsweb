import requests
import json

from ioobjectsgui.ioobjects import IoObject


api_urls = { 'base_url'   : 'http://localhost:8080/api/v1/object/',
             'object_url' : 'http://localhost:8080/api/v1/object/id/',
             'field_url'  : 'http://localhost:8080/api/v1/object/field/'

             }


class IOApi:

    @staticmethod
    def get_objects(classes=[], not_in=False) -> list:

        url = f'{api_urls["base_url"]}?classes={",".join(classes)}&notIn={not_in}'

        response = requests.get(url)

        if response.status_code == 202 or 200:
            response = json.loads(response.content)
            response = IoObject.load_dict(response)
            return response

        return []