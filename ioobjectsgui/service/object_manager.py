from ioobjectsgui.ioobjects import IoObject
from ioobjectsgui.service import api_urls

import requests
import random
import json

class ObjectManagerService:

    @staticmethod
    def get_objects_by_id(id_list: list) -> list:

        ret = list()

        for id in id_list:
            req_url = api_urls["object_url"] + id
            response = requests.get(req_url)
            response = json.loads(response.content)
            response = IoObject.load_dict([response])
            ret.append(response[0])

        return ret

    @staticmethod
    def new_object_class(name: str, pluralName: str, description:str) -> IoObject:

        ioobject = IoObject()

        ioobject.add_field(type='text',
                           field_key='name',
                           label='Name',
                           value=name
                           )

        ioobject.add_field(type='text',
                           field_key='pluralName',
                           label='Plural Name',
                           value=pluralName
                           )

        ioobject.add_field(type='text',
                           field_key='description',
                           label='Description',
                           value=description
                           )

        heads = {'Content-type': 'application/json'}
        response = requests.post(api_urls['base_url'], data=json.dumps(ioobject.__dict__), headers=heads)
        response = json.loads(response.content)
        response = IoObject.load_dict([response])

        return response

    @staticmethod
    def delete_object(object_id: str) -> bool:
        req_url = api_urls["object_url"] + object_id

        ioobject = IoObject()
        ioobject.id = object_id

        heads = {'Content-type': 'application/json'}
        response = requests.delete(req_url, data=json.dumps(ioobject.__dict__), headers=heads)
        response = json.loads(response.content)
        response = IoObject.load_dict([response])

        return response

    @staticmethod
    def new_field_to_object(id_object: str, type: str, label: str ) -> IoObject:

        ioobject = IoObject()
        ioobject.id = id_object

        field_key = label
        field_key = field_key.title()
        field_key = field_key.replace(" ", "")
        field_key = field_key[:1].lower() + field_key[1:]
        field_key = f'{field_key}_{random.randint(0,99)}'

        ioobject.add_field(type=type,
                           field_key=field_key,
                           label=label,
                           value=None
                           )

        heads = {'Content-type': 'application/json'}
        response = requests.post(api_urls['field_url'], data=json.dumps(ioobject.__dict__), headers=heads)
        response = json.loads(response.content)
        response = IoObject.load_dict([response])

        return response[0]

    @staticmethod
    def delete_field_of_object(id_object: str, id_field: str) -> IoObject:

        req_url = f'{api_urls["field_url"]}'
        ioobject = IoObject()
        ioobject.id = id_object

        ioobject.add_field(type="",
                           field_key=id_field,
                           label="",
                           value=None
                           )


        heads = {'Content-type': 'application/json'}
        response = requests.delete(req_url, data=json.dumps(ioobject.__dict__), headers=heads)
        response = json.loads(response.content)
        response = IoObject.load_dict([response])
        return response[0]